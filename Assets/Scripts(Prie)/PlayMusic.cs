﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    public AudioClip restaurant;
    public AudioClip carnival;
    public AudioClip colloseum;
    public AudioClip graveyard;
    public AudioClip park;
    public AudioClip shady;
    public AudioClip townsqr;


    public AudioSource audioSource;

    AudioClip sound;

    void OnTriggerEnter(Collider other)
    {
        if (!BattleManager.Battle)
        {
            switch (other.gameObject.tag)
            {
                case "Restaurant":
                    {
                        sound = restaurant;
                    }
                    break;
                case "Carnival":
                    {
                        sound = carnival;
                    }
                    break;
                case "Shady":
                    {
                        sound = shady;
                    }
                    break;
                case "Park":
                    {
                        sound = park;
                    }
                    break;
                case "Colloseum":
                    {
                        BattleManager.instance.battlemusic = colloseum;
                        sound = colloseum;
                    }
                    break;
                case "Town":
                    {
                        sound = townsqr;
                    }
                    break;
                case "Graveyard":
                    {
                        sound = graveyard;
                    }
                    break;
            }

            audioSource.clip = sound;
            audioSource.Play();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (!BattleManager.Battle)
        {
            audioSource.Stop();
            audioSource.clip = sound = null;
        }
    }
}