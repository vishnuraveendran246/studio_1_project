﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleTriggerManager : MonoBehaviour
{
    Animator animator;

    int helpindex;

    public Party battle1enemies;
    public Party battle2enemies;
    public Party battle3enemies;
    public Party battle4enemies;
    public Party battle5enemies;
    public Party allies;

    public Character[] battle2Charas;
    public Character[] battle3Charas;
    public Character[] battle4Charas;
    public Character[] battle5Charas;
    public GridScript grid;
    public UnityEngine.GameObject StoryTrigger2;
    public GridScript grid2;
    public UnityEngine.GameObject StoryTrigger3;
    public GridScript grid3;
    public UnityEngine.GameObject StoryTrigger4;
    public GridScript grid4;
    public UnityEngine.GameObject StoryTrigger5;
    public GridScript grid5;

    public UnityEngine.GameObject BobsCard;
    public UnityEngine.GameObject SicarrosCard;
    public UnityEngine.GameObject SinclairsCard;

    public Character Bob;
    public Character Sicarro;

    // Start is called before the first frame update
    public void Start()
    {
        BobsCard.SetActive(false);
        SicarrosCard.SetActive(false);
        SinclairsCard.SetActive(false);

        animator = GetComponentInChildren<Animator>();

        StoryTrigger2 = UnityEngine.GameObject.FindGameObjectWithTag("StoryTrigger2");
        StoryTrigger3 = UnityEngine.GameObject.FindGameObjectWithTag("StoryTrigger3");
        StoryTrigger4 = UnityEngine.GameObject.FindGameObjectWithTag("StoryTrigger4");

        StoryTrigger2.SetActive(false);
        StoryTrigger3.SetActive(false);
        StoryTrigger4.SetActive(false);
        StoryTrigger5.SetActive(false);

        battle2Charas = battle2enemies.GetComponentsInChildren<Character>();
        battle3Charas = battle3enemies.GetComponentsInChildren<Character>();
        battle4Charas = battle4enemies.GetComponentsInChildren<Character>();

        for (int i = 0; i < battle2Charas.Length; i++)
        {
            battle2Charas[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < battle3Charas.Length; i++)
        {
            battle3Charas[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < battle4Charas.Length; i++)
        {
            battle4Charas[i].gameObject.SetActive(false);
        }

        TileManager.instance.AssignTiles(grid.tiles);
    }

    public void Update()
    {
        if (BattleManager.instance.winCount == 1 && helpindex < 1)
        {
            for (int i = 0; i < battle2Charas.Length; i++)
            {
                battle2Charas[i].gameObject.SetActive(true);
            }

            helpindex++;

            StoryTrigger2.SetActive(true);

            Debug.Log("StoryTrigger 2 is active!");
        }
        if (BattleManager.instance.winCount == 2)
        {
            for (int i = 0; i < battle3Charas.Length; i++)
            {
                battle3Charas[i].gameObject.SetActive(true);
            }

            StoryTrigger3.SetActive(true);

            BobsCard.SetActive(true);

            Debug.Log("StoryTrigger 3 is active!");
        }
        if (BattleManager.instance.winCount == 3)
        {
            for (int i = 0; i < battle4Charas.Length; i++)
            {
                battle4Charas[i].gameObject.SetActive(true);
            }

            StoryTrigger4.SetActive(true);

            Debug.Log("StoryTrigger 4 is active!");
        }
        if (BattleManager.instance.winCount == 4)
        {
            for (int i = 0; i < battle4Charas.Length; i++)
            {
                battle5Charas[i].gameObject.SetActive(true);
            }

            StoryTrigger5.SetActive(true);

            SicarrosCard.SetActive(true);
            SinclairsCard.SetActive(true);

            Debug.Log("StoryTrigger 5 is active!");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "StoryTrigger2")
        {
            battle2enemies.gameObject.SetActive(true);

            BattleManager.instance.enemies = battle2enemies;

            TileManager.instance.AssignTiles(grid2.tiles);

            animator.ResetTrigger("isMoving");
            animator.SetTrigger("notMoving");

            StorySystem.instance.PlayStory();

            StoryTrigger2.SetActive(false);
        }
        if (other.tag == "StoryTrigger3")
        {
            battle3enemies.gameObject.SetActive(true);

            BattleManager.instance.enemies = battle3enemies;

            TileManager.instance.AssignTiles(grid3.tiles);

            animator.ResetTrigger("isMoving");
            animator.SetTrigger("notMoving");

            StorySystem.instance.PlayStory();

            StoryTrigger3.SetActive(false);
        }
        if (other.tag == "StoryTrigger4")
        {
            BattleManager.instance.enemies = battle4enemies;

            TileManager.instance.AssignTiles(grid4.tiles);

            animator.ResetTrigger("isMoving");
            animator.SetTrigger("notMoving");

            StorySystem.instance.PlayStory();

            StoryTrigger4.SetActive(false);
        }
        if (other.tag == "StoryTrigger5")
        {
            BattleManager.instance.enemies = battle5enemies;

            TileManager.instance.AssignTiles(grid5.tiles);

            animator.ResetTrigger("isMoving");
            animator.SetTrigger("notMoving");

            StorySystem.instance.PlayStory();

            StoryTrigger5.SetActive(false);
        }
    }
}
