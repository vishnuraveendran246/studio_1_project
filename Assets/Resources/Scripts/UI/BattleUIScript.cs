﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleUIScript : MonoBehaviour
{
    public static BattleUIScript instance = null;

    [SerializeField] UnityEngine.GameObject battleUI;
    [SerializeField] UnityEngine.GameObject moveButton;
    [SerializeField] UnityEngine.GameObject BAButton;
    [SerializeField] UnityEngine.GameObject S1Button;
    [SerializeField] UnityEngine.GameObject S2Button;
    [SerializeField] UnityEngine.GameObject PassButton;

    private void Start()
    {
        instance = this;
        battleUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (BattleManager.instance.currentChar == null && BattleManager.Battle)
        {
            HideUI();
            return;
        }

        if (BattleManager.Battle && !battleUI.activeSelf)
        {
            battleUI.SetActive(true);
            if (BattleManager.instance.currentChar.AI)
            {
                HideUI();
            }
        }
        else if (!BattleManager.Battle && battleUI.activeSelf)
        {
            battleUI.SetActive(false);
        }

        if(BattleManager.Battle && battleUI.activeSelf)
        {
            if (BattleManager.instance.currentChar.energy < BattleManager.instance.currentChar.Skill2Energy())
                S2Button.SetActive(false);
            if (BattleManager.instance.currentChar.energy < BattleManager.instance.currentChar.Skill1Energy())
                S1Button.SetActive(false);
            if (BattleManager.instance.currentChar.energy < BattleManager.instance.currentChar.AttackEnergy())
                BAButton.SetActive(false);
            if (BattleManager.instance.currentChar.energy < BattleManager.instance.currentChar.MoveEnergy())
                moveButton.SetActive(false);
        }

    }

    public void ResetUI()
    {
        moveButton.SetActive(true);
        BAButton.SetActive(true);
        S1Button.SetActive(true);
        S2Button.SetActive(true);
    }

    public void HideUI()
    {
        moveButton.SetActive(false);
        BAButton.SetActive(false);
        S1Button.SetActive(false);
        S2Button.SetActive(false);
    }
}
