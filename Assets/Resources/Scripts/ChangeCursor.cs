﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangeCursor : MonoBehaviour
{
    public PauseMenu pauseMenu;

    public Texture2D clickable;
    public Texture2D nonClickable;
    public Texture2D battleClick;
    public Texture2D attack;

    public LayerMask layer;

    RaycastHit hit;

    void Update()
    {
        if (!pauseMenu.isPaused)
        {
            if (!BattleManager.Battle)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100, layer))
                {
                    Cursor.SetCursor(clickable, Vector2.zero, CursorMode.ForceSoftware);
                }
                else
                    Cursor.SetCursor(nonClickable, Vector2.zero, CursorMode.ForceSoftware);
            }
            else
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (BattleManager.ReturnState() == State.Attack || BattleManager.ReturnState() == State.Skill1 || BattleManager.ReturnState() == State.Skill2)
                        Cursor.SetCursor(attack, Vector2.zero, CursorMode.ForceSoftware);
                    if (BattleManager.ReturnState() == State.Move)
                        Cursor.SetCursor(battleClick, Vector2.zero, CursorMode.ForceSoftware);
                }
                else
                    Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);
            }
        }
        else
            Cursor.SetCursor(clickable, Vector2.zero, CursorMode.ForceSoftware);
    }
}
