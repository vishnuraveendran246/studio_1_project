﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridScript : MonoBehaviour
{
    public HexTile[] tiles;

    // Start is called before the first frame update
    void Start()
    {
        tiles = GetComponentsInChildren<HexTile>();

        foreach (var tile in tiles)
        {
            tile.gameObject.SetActive(false);
        }
    }
}
