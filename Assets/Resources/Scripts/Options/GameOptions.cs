﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOptions
{
    public bool fullScreen;
    public int graphics;
    public int resolution;
    public float music;
    public float sfx;
}
