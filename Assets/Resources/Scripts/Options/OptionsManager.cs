﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    public Toggle fullscreenToggle;
    public Dropdown resolutionDropdown;
    public Dropdown graphicsDropdown;
    public Slider musicSlider;
    public Slider sfxSlider;
    public Button applyButton;

    public Resolution[] resolutions;

    public AudioSource musicSRC;
    public AudioSource SFXSRC;
    public Toggle fpsToggle;
    public Text fpsCounter;

    GameOptions gameOptions;

    public void OnEnable()
    {
        gameOptions = new GameOptions();
        resolutions = Screen.resolutions;

        fullscreenToggle.onValueChanged.AddListener(delegate { FullScreenToggled(); });
        resolutionDropdown.onValueChanged.AddListener(delegate { ResChanged(); });
        graphicsDropdown.onValueChanged.AddListener(delegate { GrafixChanged(); });
        musicSlider.onValueChanged.AddListener(delegate { MusicChanged(); });
        sfxSlider.onValueChanged.AddListener(delegate { SFXChanged(); });
        applyButton.onClick.AddListener(delegate { Applied(); });
        fpsToggle.onValueChanged.AddListener(delegate { FPSToggle(); } );

        foreach (var item in resolutions)
        {
            resolutionDropdown.options.Add(new Dropdown.OptionData(item.ToString()));
        }

        LoadGameOptions();
    }

    public void FullScreenToggled()
    {
        gameOptions.fullScreen = Screen.fullScreen = fullscreenToggle.isOn;
    }

    public void ResChanged()
    {
        Screen.SetResolution(resolutions[resolutionDropdown.value].width, resolutions[resolutionDropdown.value].height, Screen.fullScreen);
        gameOptions.resolution = resolutionDropdown.value;
    }

    public void GrafixChanged()
    {
        QualitySettings.masterTextureLimit = gameOptions.graphics = graphicsDropdown.value;
    }

    public void MusicChanged()
    {
        musicSRC.volume = gameOptions.music = musicSlider.value;
    }

    public void SFXChanged()
    {
        SFXSRC.volume = gameOptions.sfx = sfxSlider.value;
    }

    public void Applied()
    {
        SaveGameOptions();
    }

    public void SaveGameOptions()
    {
        string jsonData = JsonUtility.ToJson(gameOptions, true);
        File.WriteAllText(Application.persistentDataPath + "/gameOptions.json", jsonData);
    }

    public void FPSToggle()
    {
        fpsCounter.enabled = fpsToggle.isOn;
    }

    public void LoadGameOptions()
    {
        gameOptions = JsonUtility.FromJson<GameOptions>(File.ReadAllText(Application.persistentDataPath + "/gameOptions.json"));

        musicSlider.value = gameOptions.music;
        sfxSlider.value = gameOptions.sfx;
        graphicsDropdown.value = gameOptions.graphics;
        resolutionDropdown.value = gameOptions.resolution;
        fullscreenToggle.isOn = gameOptions.fullScreen;

        Screen.fullScreen = gameOptions.fullScreen;

        resolutionDropdown.RefreshShownValue();
    }
}
