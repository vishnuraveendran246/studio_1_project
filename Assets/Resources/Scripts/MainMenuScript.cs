﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public Animator transition;
    public Transform credits;
    public Transform menuangle;
    public Menu menu;

    public Material selected;
    public Material unselected;

    public UnityEngine.GameObject startobj;
    public UnityEngine.GameObject creditsobj;
    public UnityEngine.GameObject quitobj;
    public UnityEngine.GameObject backobj;

    public void Update()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000))
        {
            switch (hit.collider.name)
            {
                case "Start Game":
                    {
                        if (startobj)
                            startobj.GetComponent<Renderer>().material = selected;
                        if (creditsobj)
                            creditsobj.GetComponent<Renderer>().material = unselected;
                        if (quitobj)
                            quitobj.GetComponent<Renderer>().material = unselected;
                        if(backobj)
                            backobj.GetComponent<Renderer>().material = unselected;
                        break;
                    }
                case "Credits":
                    {
                        if (creditsobj)
                            creditsobj.GetComponent<Renderer>().material = selected;
                        if (startobj)
                            startobj.GetComponent<Renderer>().material = unselected;
                        if (quitobj)
                            quitobj.GetComponent<Renderer>().material = unselected;
                        if (backobj)
                            backobj.GetComponent<Renderer>().material = unselected;
                        break;
                    }
                case "Quit":
                    {
                        if (quitobj)
                            quitobj.GetComponent<Renderer>().material = selected;
                        if (startobj)
                            startobj.GetComponent<Renderer>().material = unselected;
                        if (creditsobj)
                            creditsobj.GetComponent<Renderer>().material = unselected;
                        if (backobj)
                            backobj.GetComponent<Renderer>().material = unselected;
                        break;
                    }
                case "Back":
                    {
                        if (quitobj)
                            quitobj.GetComponent<Renderer>().material = unselected;
                        if (startobj)
                            startobj.GetComponent<Renderer>().material = unselected;
                        if (creditsobj)
                            creditsobj.GetComponent<Renderer>().material = unselected;
                        if (backobj)
                            backobj.GetComponent<Renderer>().material = selected;
                        break;
                    }
                default:
                    {
                        if (quitobj)
                            quitobj.GetComponent<Renderer>().material = unselected;
                        if (startobj)
                            startobj.GetComponent<Renderer>().material = unselected;
                        if (creditsobj)
                            creditsobj.GetComponent<Renderer>().material = unselected;
                        if (backobj)
                            backobj.GetComponent<Renderer>().material = unselected;
                        break;
                    }
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (Physics.Raycast(ray, out hit, 1000))
            {
                switch (hit.collider.name)
                {
                    case "Start Game":
                        StartGame();
                        break;
                    case "Credits":
                        Credits();
                        break;
                    case "Quit":
                        Quit();
                        break;
                    case "Game":
                        Quit();
                        break;
                    case "Back":
                        if(menu)
                            menu.CurrentScene = menuangle;
                        break;
                }
            }
        }
    }

    public void StartGame()
    {
        StartCoroutine(LoadScene("Cutscene"));
    }
    
    public void Credits()
    {
        if(menu)
            menu.CurrentScene = credits;
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("QUITTING APP!");
    }

    public IEnumerator LoadScene(string level)
    {
        if(transition)
            transition.SetTrigger("Start");

        yield return new WaitForSeconds(13);

        SceneManager.LoadSceneAsync(level, LoadSceneMode.Single);
    }
}
