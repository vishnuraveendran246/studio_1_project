﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum State
{
    Attack,
    Skill1,
    Skill2,
    Move
}

// SINGLETON CLASS!
public class BattleManager : MonoBehaviour
{
    //=============================================================== Battle Manager Variables ===============================================================
    static public BattleManager instance = null;

    public Character currentChar;

    [SerializeField] LayerMask layerMask;
    static bool battle = false;
    bool busy = false;
    static State state;
    static public HexTile targetTile;
    static public Character targetChara;
    
    public int winCount = 0;

    public Image victoryscreen;

    [SerializeField] public Color whenHovered = Color.white;
    [SerializeField] public Color whenTargetable = Color.white;
    [SerializeField] public Color whenSelected = Color.white;
    [SerializeField] public Color whenNormal = Color.white;

    public AudioSource audioSource;
    public AudioClip battlemusic;

    UnityEngine.SceneManagement.LoadSceneMode mode;
    //=============================================================== TurnQueue Variables ===============================================================

    public List<Character> nemenemies;

    public Party allies;
    public Party enemies;

    public VictoryScript victory;

    public PauseMenu pause;

    static List<Character> allChara = new List<Character>();

    static Queue<Character> turnOrder = new Queue<Character>();

    //=============================================================== Battle Manager Functions ===============================================================

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        state = State.Move;
    }

    // Executes the function depending on state of the battle manager
    public void RecievedInput()
    {
        if (!currentChar.AI)
        {
            BattleUIScript.instance.HideUI();
            busy = true;
            switch (state)
            {
                case State.Attack:
                    currentChar.Attack(targetTile);
                    break;
                case State.Move:
                    currentChar.Move(targetTile);
                    break;
                case State.Skill1:
                    currentChar.SkillOne(targetTile);
                    break;
                case State.Skill2:
                    currentChar.SkillTwo(targetTile);
                    break;
                default:
                    break;
            }
        }
    }

    // Attack function to change state of battle manager functions
    public void Attack()
    {
        ResetEverything();
        state = State.Attack;
        TileManager.instance.FindTilesWithinRange(currentChar, currentChar.stats.attackRange);
    }

    // Skill 1 Function to change state of battle manager functions
    public void Skill1()
    {
        ResetEverything();
        state = State.Skill1;
        TileManager.instance.FindTilesWithinRange(currentChar, currentChar.stats.skill1range);
    }

    // Skill 2 Function to change state of battle manager functions
    public void Skill2()
    {
        ResetEverything();
        state = State.Skill2;
        if (currentChar.GetType() != typeof(Protagonist))
            TileManager.instance.FindTilesWithinRange(currentChar, currentChar.stats.skill2range);
        else if (currentChar.GetType() == typeof(Protagonist))
            TileManager.instance.SelfSelect(currentChar);
    }

    // Move function to change state of battle manager functions
    public void Move()
    {
        ResetEverything();
        state = State.Move;
        TileManager.instance.FindSelectableTiles(currentChar);
    }

    // Pass to send the turn to the next character in queue
    public void Pass()
    {
        ResetEverything();

        BattleUIScript.instance.ResetUI();
        InitTurnQueue();
    }

    // Get set battle function
    static public bool Battle
    {
        get { return battle; }
        set { battle = value; }
    }

    // Starts the battle after event is raised
    public void StartBattle()
    {
        if (!Battle && !pause.isPaused)
        {
            Battle = true;
            busy = true;
            TileManager.instance.GenerateHexGrid();
            NewBattle();
            InitTurnQueue();
            audioSource.clip = battlemusic;
            audioSource.Play();
        }
        else
            Debug.Log("Battle already running!");
    }

    // If the same character has energy left and is making a move after having done a move, an event raises this function
    public void NextMove()
    {
        if (currentChar != null)
            if (!currentChar.Busy)
            {
                ResetEverything();
                currentChar.CurrentTileID = currentChar.GetCurrentTile().tileID;

                if (!currentChar.AI)
                {
                    BattleUIScript.instance.ResetUI();
                    Move();
                }
                else
                {
                    if (currentChar.energy < 2)
                    {
                        BattleManager.instance.Pass();
                        return;
                    }
                    BattleUIScript.instance.HideUI();
                }
            }
    }
    
    // Resets the all targeting values to null and raises events
    public void ResetEverything()
    {
        targetChara = null;
        targetTile = null;
        TileManager.instance.ResetTiles();
        busy = false;
    }

    static public State ReturnState()
    {
        return state;
    }

    public void EndBattle()
    {
        audioSource.Stop();
        audioSource.clip = null;
        StorySystem.instance.SetPlayingFalse();
        Debug.Log("Ended battle!");
        Battle = false;
        TileManager.instance.DestroyGrid();
        turnOrder = new Queue<Character>();
        StartCoroutine(CameraScript.instance.ChangeCurrent(Player.instance.protagonist));
    }


    //=============================================================== TurnQueue Functions ===============================================================

    // Re-initializes turn queue and reorders characters 
    void InitTurnQueue()
    {
        //HealthCheck();
        allChara.Sort();

        if (turnOrder.Count == 0)
            foreach (Character unit in allChara)
                turnOrder.Enqueue(unit);

        Character temp = turnOrder.Dequeue();
        temp.RefreshEnergy();
        currentChar = null;
        StartCoroutine(CameraScript.instance.ChangeCurrent(temp));
        busy = true;
        // TODO : Add in functionality to change card material depending on character
    }

    public void AssignCurrent(Character chara)
    {
        currentChar = chara;
        busy = false;

        NextMove();
    }

    // Checks health of relevant characters and checks if the party or enemies are wiped and calls battle to an end
    void HealthCheck()
    {
        int allyDead = 0;
        int enemyDead = 0;

        foreach (var member in allies.Members())
        {
            if (member != null)
            {
                if (member.health <= 0)
                {
                    allyDead++;
                    if (allChara.Contains(member))
                    {
                        member.GetCurrentTile().occupant = null;
                        member.GetCurrentTile().Occupied = false;
                        member.gameObject.SetActive(false);
                        allChara.Remove(member);
                        allies.DeleteMember(member);
                    }
                    BattleManager.instance.Pass();
                }
            }
        }

        foreach (var member in enemies.Members())
        {
            if (member != null)
            {
                if (member.health <= 0)
                {
                    enemyDead++;
                    if (allChara.Contains(member))
                    {
                        member.GetCurrentTile().occupant = null;
                        member.GetCurrentTile().Occupied = false;
                        member.gameObject.SetActive(false);
                        allChara.Remove(member);
                        enemies.DeleteMember(member);
                    }
                    BattleManager.instance.Pass();
                }
            }
        }

        if (allyDead == allies.Members().Count)
        {
            Debug.Log("Lost the Battle!");
            EndBattle();
            allyDead = 0;
            SceneManager.LoadScene("Scheletro", mode = LoadSceneMode.Single);
        }

        if (enemyDead == enemies.Members().Count)
        {
            Debug.Log("Won the Battle!");
            enemyDead = 0;
            winCount++;
            victory.Victory();
            EndBattle();
        }
    }

    // Starts new battle
    public void NewBattle()
    {
        turnOrder.Clear();

        allChara = new List<Character>();

        allies = Player.instance.allies;

        foreach (var ally in allies.Members())
        {
            if (ally != null)
            {
                ally.Initialize();
                ally.MoveToNearestTile();
                allChara.Add(ally);
            }
        }

        // Need to find way to deal with Spawning enemies through scriptable objects

        foreach (var enemy in enemies.Members())
        {
            if (enemy != null)
            {
                enemy.Initialize();
                enemy.MoveToNearestTile();
                allChara.Add(enemy);
            }
        }

        var tempAllyArray = allies.Members();
        var tempEnemyArray = enemies.Members();


        for (int i = 0; i < allies.Members().Count; i++)
        {
            tempAllyArray[i].transform.LookAt(tempEnemyArray[i].transform.position);
            tempEnemyArray[i].transform.LookAt(tempAllyArray[i].transform.position);

            tempEnemyArray[i].animator.SetTrigger("notMoving");
            tempEnemyArray[i].animator.ResetTrigger("isMoving");

            tempAllyArray[i].animator.SetTrigger("notMoving");
            tempAllyArray[i].animator.ResetTrigger("isMoving");
        }
    }

    public void Update()
    {
        if (currentChar != null && !busy)
            currentChar.BattleUpdate();

        if(Battle && !busy)
            HealthCheck();
    }
}