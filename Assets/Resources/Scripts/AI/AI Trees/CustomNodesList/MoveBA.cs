﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class MoveBA : AITreeNode
{
    public override AITreeNodeState Execute()
    {
        Debug.Log("Moving to BA Range!");

        Character thisChar = BattleManager.instance.currentChar;

        BattleManager.instance.Move();
        thisChar.Move(thisChar.myTree.tileToMoveTo);

        if (thisChar.GetCurrentTile() != thisChar.myTree.tileToMoveTo)
        {
            thisChar.animator.SetTrigger("Move");
            return AITreeNodeState.Running;
        }
        else
        {
            thisChar.animator.SetTrigger("NotMove");
            thisChar.animator.ResetTrigger("Move");
        }

        return AITreeNodeState.Succeeded;
    }
}
