﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AIBasic : AITreeNode
{
    public override AITreeNodeState Execute()
    {
        Character thisChar = BattleManager.instance.currentChar;

        Debug.Log("Using BA!");

        BattleManager.instance.Attack();
        thisChar.Attack(AITree.AIstarget.GetCurrentTile());

        if (thisChar.Busy)
        {
            return AITreeNodeState.Running;
        }

        return AITreeNodeState.Succeeded;
    }
}