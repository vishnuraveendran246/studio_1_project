﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AIS2 : AITreeNode
{
    public override AITreeNodeState Execute()
    {
        Character thisChar = BattleManager.instance.currentChar;

        Debug.Log("Using S2!");

        BattleManager.instance.Skill2();
        thisChar.SkillTwo(AITree.AIstarget.GetCurrentTile());

        if (thisChar.Busy)
        {
            return AITreeNodeState.Running;
        }

        return AITreeNodeState.Succeeded;
    }
}