﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeCheck : AITreeNode
{
    private AITreeNode child;
    private string attack;

    public RangeCheck(AITreeNode node, string msg)
    {
        child = node;
        attack = msg;
    }

    public override AITreeNodeState Execute()
    {
        Character currChar = BattleManager.instance.currentChar;

        switch (attack)
        {
            case "BA":
                {
                    List<HexTile> temp = TileManager.instance.ReturnTilesWithinRangeToAI(currChar, currChar.stats.attackRange);

                    for (int i = 0; i < temp.Count; i++)
                    {
                        if (temp[i].occupant == AITree.AIstarget)
                        {
                            return child.Execute();
                        }
                    }
                    return AITreeNodeState.Failed;
                }
            case "S1":
                {
                    List<HexTile> temp = TileManager.instance.ReturnTilesWithinRangeToAI(currChar, currChar.stats.skill1range);

                    for (int i = 0; i < temp.Count; i++)
                    {
                        if (temp[i].occupant == AITree.AIstarget)
                        {
                            return child.Execute();
                        }
                    }
                    return AITreeNodeState.Failed;
                }
            case "S2":
                {
                    List<HexTile> temp = TileManager.instance.ReturnTilesWithinRangeToAI(currChar, currChar.stats.skill2range);

                    for (int i = 0; i < temp.Count; i++)
                    {
                        if (temp[i].occupant == AITree.AIstarget)
                        {
                            return child.Execute();
                        }
                    }
                    return AITreeNodeState.Failed;
                }
            default: return AITreeNodeState.Failed;
        }
    }
}
