﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AIS1 : AITreeNode
{
    public override AITreeNodeState Execute()
    {
        Character thisChar = BattleManager.instance.currentChar;

        Debug.Log("Using S1!");

        BattleManager.instance.Skill1();
        BattleManager.instance.currentChar.SkillOne(AITree.AIstarget.GetCurrentTile());

        if (thisChar.Busy)
        {
            return AITreeNodeState.Running;
        }

        return AITreeNodeState.Succeeded;
    }
}