﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyCheck : AITreeNode
{
    private AITreeNode child;
    private string attack;

    public EnergyCheck(AITreeNode node, string msg)
    {
        child = node;
        attack = msg;
    }

    public override AITreeNodeState Execute()
    {
        switch (attack)
        {
            case "BA":
                {
                    if (BattleManager.instance.currentChar.energy >= BattleManager.instance.currentChar.AttackEnergy())
                    {
                        return child.Execute();
                    }
                    else
                        return AITreeNodeState.Failed;
                }
            case "S1":
                {
                    if (BattleManager.instance.currentChar.energy >= BattleManager.instance.currentChar.Skill1Energy())
                    {
                        return child.Execute();
                    }
                    else
                        return AITreeNodeState.Failed;
                }
            case "S2":
                {
                    if (BattleManager.instance.currentChar.health < BattleManager.instance.currentChar.baseStats.health / 2)
                    {
                        if (BattleManager.instance.currentChar.energy >= BattleManager.instance.currentChar.Skill2Energy())
                        {
                            return child.Execute();
                        }

                    }
                    return AITreeNodeState.Failed;
                }
            case "Move":
                {
                    if (BattleManager.instance.currentChar.energy > 0)
                        return AITreeNodeState.Succeeded;
                    else
                        return AITreeNodeState.Failed;
                }
            default: return AITreeNodeState.Failed;
        }
    }
}
