﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EndTurn : AITreeNode
{
    public override AITreeNodeState Execute()
    {
        Debug.Log("AI Cannot do anything");

        BattleManager.instance.Pass();

        return AITreeNodeState.Succeeded;
    }
}
