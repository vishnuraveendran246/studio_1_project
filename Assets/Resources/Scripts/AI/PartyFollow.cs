﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PartyFollow : MonoBehaviour
{
    public Transform toFollow;
    public PauseMenu pauseMenu;
    public float distToTarget;
    public float allowedDist;
    public float followSpeed;

    private float speed;

    public RaycastHit hit;

    public Animator animator;

    public Character character;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        character = GetComponent<Character>();
    }

    void Update()
    {
        if (!pauseMenu.isPaused && !BattleManager.Battle && BattleManager.instance.allies.Members().Contains(character))
        {
            transform.LookAt(toFollow.transform);

            distToTarget = Vector3.Distance(transform.position, toFollow.transform.position);
            if (distToTarget >= allowedDist)
            {
                speed = followSpeed;
                animator.SetTrigger("isMoving");
                animator.ResetTrigger("notMoving");
                transform.position = Vector3.MoveTowards(transform.position, toFollow.transform.position, speed);
            }
            else
            {
                speed = 0;
                animator.SetTrigger("notMoving");
                animator.ResetTrigger("isMoving");
            }
        }
    }
}
