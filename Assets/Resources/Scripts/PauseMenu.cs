﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public bool isPaused;

    public UnityEngine.GameObject pauseUI;

    public UnityEngine.GameObject partySelectMenu;
    public UnityEngine.GameObject optionsMenu;

    UnityEngine.SceneManagement.LoadSceneMode mode;

    private void Start()
    {
        isPaused = false;
        if (pauseUI)
            pauseUI.SetActive(false);
        if (partySelectMenu)
            partySelectMenu.SetActive(false);
        if (optionsMenu)
            optionsMenu.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                if (pauseUI.activeSelf)
                    Resume();
            }
            else
            {
                Pause();
                isPaused = true;
            }
        }

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (Physics.Raycast(ray, out hit, 1000))
            {
                switch (hit.collider.name)
                {
                    case "Exit": 
                        SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);
                        break;
                }
            }
        }
    }

    public void Resume()
    {
        Time.timeScale = 1f;
        pauseUI.SetActive(false);
        isPaused = false;
    }
    public void Pause()
    {
        PartySelect();
        Time.timeScale = 0f;
        pauseUI.SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
        Debug.LogError("QUITTING APP!");
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Scheletro", LoadSceneMode.Single);
    }

    public void PartySelect()
    {
        Debug.Log("Party Selection!");
        optionsMenu.SetActive(false);
        partySelectMenu.SetActive(true);
        //pauseUI.SetActive(false);
    }

    public void OptionsMenu()
    {
        Debug.Log("Option Menu!");
        partySelectMenu.SetActive(false);
        optionsMenu.SetActive(true);
        //pauseUI.SetActive(false);
    }
}
