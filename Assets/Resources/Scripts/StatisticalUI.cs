﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatisticalUI : MonoBehaviour
{
    Text textforStats;
    
    // Start is called before the first frame update
    void Start()
    {
        textforStats = this.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (BattleManager.instance.currentChar != null)
        textforStats.text = "Current character: " + BattleManager.instance.currentChar.name + "\n" + "Health: " + BattleManager.instance.currentChar.health + "\n" + "Energy:" + BattleManager.instance.currentChar.energy;
    }
}
