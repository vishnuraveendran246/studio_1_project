﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public abstract class Character : MonoBehaviour, IComparable
{
    public BaseStats baseStats;
    public CoolDowns baseCD;
    public CoolDowns CD;
    public int energy;
    public float health;
    public Stats stats;

    public Animator animator;

    public AITree myTree;

    public bool AI = false;
    bool targetable = false;

    public bool Busy = false;

    public static float checkTileRange = 1.5f;

    HexTile currentTile = null;

    public int CurrentTileID;

    public Sprite profileCard;
    public Sprite BACard;
    public Sprite S1Card;
    public Sprite S2Card;

    public Image profile;
    public Image BA;
    public Image S1;
    public Image S2;

    public Vector3 targetPos;

    public abstract void Move(HexTile tile);
    public abstract void Attack(HexTile tile);
    public abstract void SkillOne(HexTile tile);
    public abstract void SkillTwo(HexTile tile);
    public abstract int MoveEnergy();
    public abstract int AttackEnergy();
    public abstract int Skill1Energy();
    public abstract int Skill2Energy();

    private void Start()
    {
        animator = this.gameObject.GetComponentInChildren<Animator>();
    }

    public void BattleUpdate()
    {
        BA.sprite = BACard;
        S1.sprite = S1Card;
        S2.sprite = S2Card;
        profile.sprite = profileCard;

        if (BattleManager.Battle)
            CurrentTileID = GetCurrentTile().tileID;
    
        if (BattleManager.Battle && AI && !Busy)
        {
            Busy = true;
            myTree.Execute();
        }
    }

    // Instantiates and assigns Scriptable object
    public void Initialize()
    {
        health = baseStats.health;
        energy = baseStats.energy;
        stats = ScriptableObject.CreateInstance<Stats>();
        CD = ScriptableObject.CreateInstance<CoolDowns>();
        CD.Reset();
        stats.Copy(baseStats);
    }

    // Returns current tile of the character
    public HexTile GetCurrentTile()
    {
        return currentTile;
    }

    // Moves the character to the nearest tile (Called after hexgrid is generated when battle event is raised)
    public void MoveToNearestTile()
    {
        HexTile destination = ReturnNearestUnoccupiedTile();
        currentTile = destination;
        currentTile.occupant = this;
        StartCoroutine(MoveToTile(destination));
    }

    // Coroutine for smoothly moving character to a tile
    protected IEnumerator MoveToTile(HexTile destination)
    {
        Busy = true;
        Vector3 currentPos = transform.position;
        Vector3 destPos = destination.ReturnTargetPosition(currentPos);
        float time = 0;

        while (time < 1)
        {
            transform.position = Vector3.Lerp(currentPos, destPos, time);
            time += Time.deltaTime * 4/3;
            yield return null;
        }

        transform.position = destination.ReturnTargetPosition(transform.position);

        currentTile = destination;
        currentTile.Walkable = false;
        Busy = false;
        yield return null;
    }
    
    // Compare function for IComparable, ordering by speed of the characters
    public int CompareTo(object obj)
    {
        Character chara = (Character)obj;
        if (stats.speed > chara.stats.speed)
            return 1;
        else if (stats.speed == chara.stats.speed)
            return 0;
        else
            return -1;
    }

    // Returns nearest Unoccupied tile
    HexTile ReturnNearestUnoccupiedTile()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, checkTileRange);
        HexTile temp = null;
        HexTile lowest = null;
        foreach (var item in hitColliders)
        {
            temp = item.GetComponent<HexTile>();
            if (temp != null)
            {
                if (lowest == null)
                    lowest = temp;
                else if (!temp.Occupied)
                {
                    if (Vector3.Distance(transform.position, temp.transform.position) < Vector3.Distance(transform.position, lowest.transform.position))
                        lowest = temp;
                }
            }
        }

        if (lowest != null)
            lowest.Occupied = true;
        return lowest;
    }

    // Refils energy of the character
    public void RefreshEnergy()
    {
        energy = baseStats.energy;
    }

    public void SkillPush(HexTile tile)
    {
        StartCoroutine(MoveToTile(tile));
    }

    // Coroutine for smoothly moving character down a list of tiles
    protected IEnumerator MoveDownPath(List<HexTile> path)
    {
        Busy = true;
        float time = 0;
        foreach (var tile in path)
        {
            if (tile != currentTile)
            {
                Vector3 currentPos = transform.position;
                Vector3 destPos = tile.ReturnTargetPosition(currentPos);
                while (time <= 1)
                {
                    transform.LookAt(new Vector3(tile.ReturnTargetPosition(currentPos).x, transform.position.y, tile.ReturnTargetPosition(currentPos).z));
                    transform.position = Vector3.Lerp(currentPos, tile.ReturnTargetPosition(currentPos), time);
                    time += Time.deltaTime * 4/3;
                    yield return null;
                }               

                if (time >= 1)
                {                    
                    time = 0;
                }
            }
        }

        transform.position = path[path.Count - 1].ReturnTargetPosition(transform.position);

        currentTile.Occupied = false;
        currentTile = path[path.Count - 1];
        currentTile.Occupied = true;
        currentTile.Walkable = false;
        currentTile.occupant = this;
        Busy = false;

        animator.SetTrigger("NotMove");
        animator.ResetTrigger("Move");

        BattleManager.instance.NextMove();
        yield return null;
    }

    protected void AIMoveAcrossPath(List<HexTile> path)
    {
        Busy = true;
        float time = 0;
        foreach (var tile in path)
        {
            if (tile != currentTile)
            {
                Vector3 currentPos = transform.position;
                Vector3 destPos = tile.ReturnTargetPosition(currentPos);
                while (time < 1)
                {
                    transform.LookAt(new Vector3(tile.ReturnTargetPosition(currentPos).x, transform.position.y, tile.ReturnTargetPosition(currentPos).z));
                    transform.position = Vector3.Lerp(currentPos, tile.ReturnTargetPosition(currentPos), time);
                    time += Time.unscaledDeltaTime * stats.speed;
                }

                if (time >= 1)
                    time = 0;
            }
        }

        transform.position = path[path.Count - 1].ReturnTargetPosition(transform.position);

        currentTile.Occupied = false;
        currentTile = path[path.Count - 1];
        currentTile.Occupied = true;
        currentTile.Walkable = false;
        currentTile.occupant = this;
        BattleManager.instance.NextMove();
        Busy = false;
    }

    protected IEnumerator ReturnTimer()
    {
        Debug.Log("Wait timer running");
        yield return new WaitForSeconds(5f);
        Debug.Log("Wait timer done");
        BattleManager.instance.NextMove();
        Busy = false;
        yield return null;
    }
}