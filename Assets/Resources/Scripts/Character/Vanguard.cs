﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vanguard : Character
{
    private float TimeToFire;

    // Start is called before the first frame update
    void Start()
    {
        animator = this.gameObject.GetComponentInChildren<Animator>();

        Initialize();
        myTree = new AITree(new Selector(new List<AITreeNode> { new Sequence(new List<AITreeNode> { new AttackMostFatal(), new Selector(new List<AITreeNode>
        { new EnergyCheck(new RangeCheck(new AIS2(), "S2"), "S2"), new EnergyCheck(new RangeCheck(new AIS1(), "S1"), "S1"), new EnergyCheck(new RangeCheck(new AIBasic(), "BA"), "BA") }) }),
            new Sequence(new List<AITreeNode> { new AttackLowest(), new Selector(new List<AITreeNode>
        { new EnergyCheck(new RangeCheck(new AIS2(), "S2"), "S2"), new EnergyCheck(new RangeCheck(new AIS1(), "S1"), "S1"), new EnergyCheck(new RangeCheck(new AIBasic(), "BA"), "BA") }) }),
            new Sequence(new List<AITreeNode> { new AttackNearest(), new Selector(new List<AITreeNode>
        { new EnergyCheck(new RangeCheck(new AIS2(), "S2"), "S2"), new EnergyCheck(new RangeCheck(new AIS1(), "S1"), "S1"), new EnergyCheck(new RangeCheck(new AIBasic(), "BA"), "BA") }) }),
             new Sequence(new List<AITreeNode>{new PickATile(), new MoveBA() }),
            new EndTurn()}));
    }

    public override void Move(HexTile tile)
    {
        animator.SetTrigger("Move");

        StartCoroutine(MoveDownPath(Pathfinder.instance.FindPath(GetCurrentTile(), tile)));

        energy -= tile.energyCost;
    }

    public override void Attack(HexTile tile)
    {
        Character target = tile.occupant;

        if (target != null)
        {
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));

            animator.SetTrigger("BA");

            Debug.Log(this.name + " is attacking the " + target.name + "!");

            target.health -= 10;

            Debug.Log(target.name + "'s health is now: " + target.health);

            energy -= AttackEnergy();
        }

        StartCoroutine(ReturnTimer());
    }

    public override void SkillOne(HexTile tile)
    {
        Character target = tile.occupant;

        if (target != null)
        {
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));

            animator.SetTrigger("S1");

            Debug.Log(this.name + " is attacking the " + target.name + "!");

            target.health -= 15;

            Debug.Log(target.name + "'s health is now: " + target.health);

            energy -= Skill1Energy();
        }

        StartCoroutine(ReturnTimer());

    }

    public override void SkillTwo(HexTile tile)
    {
        Character target = tile.occupant;

        if (target != null)
        {
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));

            animator.SetTrigger("S2");

            Debug.Log(this.name + " is attacking the " + target.name + "!");

            target.health -= 25;

            Debug.Log(target.name + "'s health is now: " + target.health);

            energy -= Skill2Energy();
        }

        StartCoroutine(ReturnTimer());
    }

    public override int MoveEnergy()
    {
        return 2;
    }

    public override int AttackEnergy()
    {
        return 8;
    }

    public override int Skill1Energy()
    {
        return stats.skill1Energy;
    }

    public override int Skill2Energy()
    {
        return stats.skill2Energy;
    }
}
