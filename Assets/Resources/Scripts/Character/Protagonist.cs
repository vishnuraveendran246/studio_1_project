﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Protagonist : Character
{
    float maxHealth;

    void Start()
    {
        animator = this.gameObject.GetComponentInChildren<Animator>();

        maxHealth = this.baseStats.health;

        Initialize();
    }

    public override void Move(HexTile tile)
    {
        animator.SetTrigger("Move");

        StartCoroutine(MoveDownPath(Pathfinder.instance.FindPath(GetCurrentTile(), tile)));

        energy -= tile.energyCost;
    }

    public override void Attack(HexTile tile)
    {
        Character target = tile.occupant;

        if (target != null)
        {
            targetPos = target.transform.position;

            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));

            animator.SetTrigger("BA");

            Debug.Log(this.name + " is attacking the " + target.name + "!");

            target.health -= 10;

            Debug.Log(target.name + "'s health is now: " + target.health);

            energy -= AttackEnergy();
        }

        StartCoroutine(ReturnTimer());
    }

    public override void SkillOne(HexTile tile)
    {
        Character target = tile.occupant;

        if (target != null)
        {
            targetPos = target.transform.position;

            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));

            animator.SetTrigger("S1");

            Debug.Log(this.name + " is attacking the " + target.name + "!");

            target.health -= 50;

            Debug.Log(target.name + "'s health is now: " + target.health);

            energy -= Skill1Energy();
        }

        StartCoroutine(ReturnTimer());
    }

    public override void SkillTwo(HexTile tile)
    {
        if (BattleManager.instance.currentChar.health < maxHealth)
        {
            BattleManager.instance.currentChar.health += 20;

            animator.SetTrigger("S2");

            energy -= Skill2Energy();

            Debug.Log(this.name + " is healing!");

        }
        StartCoroutine(ReturnTimer());
    }

    public override int MoveEnergy()
    {
        return 2;
    }

    public override int AttackEnergy()
    {
        return 6;
    }

    public override int Skill1Energy()
    {
        return stats.skill1Energy;
    }

    public override int Skill2Energy()
    {
        return stats.skill2Energy;
    }
}
