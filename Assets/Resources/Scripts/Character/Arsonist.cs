﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arsonist : Character
{
    void Start()
    {
        animator = this.gameObject.GetComponentInChildren<Animator>();

        Initialize();
        myTree = new AITree(new Selector(new List<AITreeNode> { new Sequence(new List<AITreeNode> { new AttackLowest(), new Selector(new List<AITreeNode>
        { new EnergyCheck(new RangeCheck(new AIS1(), "S1"), "S1"), new EnergyCheck(new RangeCheck(new AIBasic(), "BA"), "BA") }) }),
            new Sequence(new List<AITreeNode> { new AttackNearest(), new Selector(new List<AITreeNode>
        { new EnergyCheck(new RangeCheck(new AIS1(), "S1"), "S1"), new EnergyCheck(new RangeCheck(new AIBasic(), "BA"), "BA") }) }),
             new Sequence(new List<AITreeNode>{new PickATile(), new MoveBA() }),
            new EndTurn()}));
    }

    public override void Move(HexTile tile)
    {
        animator.SetTrigger("Move");

        StartCoroutine(MoveDownPath(Pathfinder.instance.FindPath(GetCurrentTile(), tile)));

        energy -= tile.energyCost;
    }

    public override void Attack(HexTile tile)
    {
        Character target = tile.occupant;

        if (target != null)
        {
            targetPos = target.transform.position;

            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
                        
            animator.SetTrigger("BA");

            Debug.Log(this.name + " is attacking the " + target.name + "!");

            target.health -= 4;

            Debug.Log(target.name + "'s health is now: " + target.health);

            energy -= AttackEnergy();
        }

        StartCoroutine(ReturnTimer());
    }

    public override void SkillOne(HexTile tile)
    {
        targetPos = tile.transform.position;

        transform.LookAt(new Vector3(tile.transform.position.x, transform.position.y, tile.transform.position.z));

        animator.SetTrigger("S1");

        Debug.Log("I am using Skill 1!");

        List<HexTile> affectedTiles = tile.ReturnNeighbours();

        List<Character> affectedCharacters = new List<Character>();

        //#Loop to make list of affected characters
        //---------------------------------------------------
        for (int i = 0; i < affectedCharacters.Count; i++)
        {
            affectedCharacters.Add(affectedTiles[i].occupant);
        }
        //---------------------------------------------------
        
        tile.occupant.health -= 6;


        //#Loop to damage affected characters
        //---------------------------------------------------
        for (int i = 0; i < affectedCharacters.Count; i++)
        {
            affectedCharacters[i].health -= 3;
        }
        //---------------------------------------------------
        
        energy -= Skill1Energy();

        StartCoroutine(ReturnTimer());
    }

    public override void SkillTwo(HexTile tile)
    {
        transform.LookAt(new Vector3(tile.transform.position.x, transform.position.y, tile.transform.position.z));

        animator.SetTrigger("S2");

        float timee = 0;
        
        timee +=Time.deltaTime;

        Debug.Log("I am using Skill2!");

        List<HexTile> affectedTiles = tile.ReturnNeighbours();

        List<Character> affectedCharacters = new List<Character>();

        //#Loop to make list of affected characters
        //---------------------------------------------------
        for (int i = 0; i < affectedCharacters.Count; i++)
        {
            affectedCharacters.Add(affectedTiles[i].occupant);
        }
        //---------------------------------------------------

        this.SkillPush(tile);   //Moves to selected tile

        float reduceBy = this.baseStats.health / this.health;  //Reduces damage inflicted depending on current health

        //#Loop to damage affected characters
        //---------------------------------------------------
        for (int i = 0; i < affectedCharacters.Count; i++)
        {
            Debug.Log("Affected: " + affectedCharacters[i].name + "\n Reduced by:" + reduceBy);
            affectedCharacters[i].health -= 8/reduceBy;
        }
        //---------------------------------------------------

        this.health = 0;   //Damages self

        energy -= Skill2Energy();

        this.GetCurrentTile().Occupied = false;

        StartCoroutine(ReturnTimer());
    }

    public override int MoveEnergy()
    {
        return 2;
    }

    public override int AttackEnergy()
    {
        return 4;
    }

    public override int Skill1Energy()
    {
        return stats.skill1Energy;
    }

    public override int Skill2Energy()
    {
        return stats.skill2Energy;
    }
}
