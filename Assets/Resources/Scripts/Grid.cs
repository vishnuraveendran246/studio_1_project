﻿using UnityEditor;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour
{
    public Transform targetPos;
    public UnityEngine.GameObject hexTile;
    public Vector2 dim;

    public float radius = 0.5f;
    public bool useAsInnerCircleRadius = true;
    private float offsetX, offsetZ;

    List<HexTile> tileList = new List<HexTile>();

    public void GenerateGrid()
    {
        tileList.Clear();

        float unitLength;
        if (useAsInnerCircleRadius)
            unitLength = (radius / (Mathf.Sqrt(3) / 2));
        else
            unitLength = radius;
        offsetX = unitLength * Mathf.Sqrt(3);
        offsetZ = unitLength * 1.5f;
        
        for (int i = -(int)dim.x / 2; i < dim.x / 2; i++)
        {
            for (int j = -(int)dim.y / 2; j < dim.y / 2; j++)
            {
                Vector2 hexpos = HexOffset(i, j);
                Vector3 pos = new Vector3(hexpos.x + targetPos.position.x, targetPos.position.y, hexpos.y + targetPos.position.z);
                tileList.Add(Instantiate(hexTile, pos, Quaternion.identity).GetComponent<HexTile>());
            }
        }
    }

    public void ClearGrid()
    {
        for (int i = tileList.Count - 1; i >= 0; i--)
        {
            DestroyImmediate(tileList[i].gameObject);
        }

        tileList.Clear();
    }

    // Calculates HexOffset in relation to passed values for finding position of generated tile
    Vector2 HexOffset(int x, int z)
    {
        Vector2 position = Vector3.zero;
        if (z % 2 == 0)
        {
            position.x = x * offsetX;
            position.y = z * offsetZ;
        }
        else
        {
            position.x = (x + 0.5f) * offsetX;
            position.y = z * offsetZ;
        }
        return position;
    }
}