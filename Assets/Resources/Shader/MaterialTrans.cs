﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialTrans : MonoBehaviour
{
    Renderer renderer;

    Material[] mats;

    Color color;
    public float minDistance = 30f;
    public float transVal = 0.02f;

    float distance;
    float time = 1;
    bool function = false;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        mats = renderer.sharedMaterials;
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(Camera.main.transform.position, transform.position);

        if (distance <= minDistance)
            color.a = transVal;
        else
            color.a = 1;
    }

    private void LateUpdate()
    {
        for (int i = 0; i < mats.Length; i++)
        {
            mats[i].SetColor("_Color", color);
        }
    }
}
