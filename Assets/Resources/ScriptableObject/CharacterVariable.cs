﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CharacterVariable : ScriptableObject
{
    public Character character;
}
