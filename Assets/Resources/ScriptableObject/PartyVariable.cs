﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PartyVariable : ScriptableObject
{
    public Character[] members;
}
