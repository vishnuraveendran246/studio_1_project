﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CharacterListVariable : ScriptableObject
{
    public List<Character> list;
}
