﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class StoryVariable : ScriptableObject
{
    public Story story = null;
}
