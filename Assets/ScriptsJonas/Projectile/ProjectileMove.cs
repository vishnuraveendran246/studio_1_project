﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMove : MonoBehaviour
{
    const float gravity = 5f;
    public float Speed;
    public float FireRate;
    public UnityEngine.GameObject MuzzlePrefab;
    public UnityEngine.GameObject HitPrefab;

    public bool arsonist = false;
    public Vector3 targetPos;
    Vector3 startPos;

    float time = 0;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;

        if (MuzzlePrefab != null)
        {
            var muzzleVFX = Instantiate(MuzzlePrefab,transform.position,Quaternion.identity);
            muzzleVFX.transform.forward = gameObject.transform.forward;

            var psMuzzle = muzzleVFX.GetComponent<ParticleSystem>();

            if(psMuzzle != null)
            {
                Destroy(muzzleVFX, psMuzzle.main.duration);
            }
            else
            {
                var psChild = muzzleVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(muzzleVFX, psChild.main.duration);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if (Speed != 0)
        {
            if (arsonist)
            {
                float distance = Vector3.Distance(startPos, targetPos);

                float angle =  Mathf.Asin(distance * gravity / (Speed * Speed) ) / 2;

                Vector3 currentPos = transform.position;

                currentPos += transform.forward * Mathf.Cos(angle) * Time.deltaTime * Speed;
                currentPos += transform.up * (Speed * Mathf.Sin(angle) * Time.deltaTime - gravity * time * Time.deltaTime);

                transform.position = currentPos;
            }
            else
                transform.position += transform.forward * (Speed * Time.deltaTime);
        }
        else
        {
            Debug.Log("No Speed");
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Delete(collision);
    }

    public void Delete(Collision collision)
    {
        Speed = 0;

        ContactPoint contact = collision.contacts[0];
        Quaternion rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 position = contact.point;

        if (HitPrefab != null)
        {
            var hitVFX = Instantiate(HitPrefab, position, rotation);

            var HitPS = hitVFX.GetComponent<ParticleSystem>();

            if (HitPS != null)
            {
                Destroy(hitVFX, HitPS.main.duration);
            }
            else
            {
                var psChild = hitVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitVFX, psChild.main.duration);
            }
        }

        Destroy(gameObject);
    }
}
