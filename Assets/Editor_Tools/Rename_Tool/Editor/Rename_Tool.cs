﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class Rename_Tool : EditorWindow
{
    public string newName = "";

    GUIStyle textStyle = new GUIStyle();

    [MenuItem("Editor Tools/Rename Tool", false, 1)]
    static void Init()
    {
        Rename_Tool renameGUI = (Rename_Tool)EditorWindow.GetWindow(typeof(Rename_Tool));

        renameGUI.titleContent = new GUIContent("Rename");
        renameGUI.minSize = new Vector2(200, 300);
        renameGUI.Show();
    }

    void OnGUI()
    {
        EditorGUIUtility.labelWidth = 60;
        
        EditorGUILayout.LabelField("Current Name: " + Selection.activeGameObject.name);

        GUILayout.BeginVertical();
        {
            newName = EditorGUILayout.TextField("Name: " , newName);
        }

        if(GUILayout.Button("Rename", GUILayout.Height(30)))
        {
            Rename();
        }

        GUILayout.EndVertical();

        Repaint();
    }

    void Rename()
    {
        if (Selection.activeObject)
        {
            UnityEngine.Object selected = Selection.activeObject;

            if (newName != "")
            {
                if (!AssetDatabase.Contains(selected))
                {
                    Selection.activeObject.name = newName;
                }
                else
                {
                    string assetPath = AssetDatabase.GetAssetPath(selected);
                    AssetDatabase.RenameAsset(assetPath, newName);
                }
            }
            else
                EditorUtility.DisplayDialog("RENAMING ERROR!", "PLEASE PROVIDE A NAME!", "OK");
        }
        else
            EditorUtility.DisplayDialog("RENAMING ERROR!", "PLEASE SELECT AN OBJECT!", "OK");
    }
}
