﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class Cutscene : MonoBehaviour
{
    public string sceneToTransitionTo; 
    VideoPlayer video;

    private void Start()
    {
        video = GetComponent<VideoPlayer>();
        StartCoroutine(WaitFunction());
    }

    IEnumerator WaitFunction()
    {
        yield return new WaitForSeconds((float)video.length * video.playbackSpeed + 1f);
        SceneManager.LoadSceneAsync(sceneToTransitionTo, LoadSceneMode.Single);
    }
}
