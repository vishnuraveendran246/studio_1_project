﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootVFXScript : MonoBehaviour
{
    private UnityEngine.GameObject effectToSpawn;
    public UnityEngine.GameObject FirePoint;

    public UnityEngine.GameObject vfx;

    public Character chara;

    public void SpawnVFX()
    {
        if (FirePoint != null)
        {
            UnityEngine.GameObject temp = Instantiate(vfx, FirePoint.transform.position, Quaternion.identity);

            if (FirePoint != null)
            {
                temp.transform.localRotation = FirePoint.transform.rotation;

            }

            if (chara != null)
            {
                temp.transform.LookAt(new Vector3(chara.targetPos.x, chara.targetPos.y + 1, chara.targetPos.z));

                if (chara.GetType() == typeof(Arsonist))
                {
                    ProjectileMove ars = temp.GetComponent<ProjectileMove>();
                    ars.arsonist = true;
                    ars.targetPos = new Vector3(chara.targetPos.x, chara.targetPos.y + 1, chara.targetPos.z);
                }
            }
        }
        else
        {
            Debug.Log("No Fire Point");
        }
    }
}
