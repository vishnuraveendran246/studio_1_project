﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIElement : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Animator animator;

    private bool mouse_over = false;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        animator.SetBool("isHover", true);
        mouse_over = true;
        Debug.Log("Mouse enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        animator.SetBool("isHover", false);
        mouse_over = false;
        Debug.Log("Mouse exit");
    }
}
