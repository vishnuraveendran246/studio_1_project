﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardSlot : MonoBehaviour, IDropHandler
{    
    public void OnDrop(PointerEventData pointerEventData)
    {
        Debug.Log("Dropped");
        if (pointerEventData.pointerDrag != null)
        {
            pointerEventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
            if (this.name == "PartySlot1" || this.name == "PartySlot2")
            {
                BattleManager.instance.allies.AddMember(pointerEventData.pointerDrag.GetComponent<DragNDrop>().character);
            }
            else
            {
                BattleManager.instance.allies.DeleteMember(pointerEventData.pointerDrag.GetComponent<DragNDrop>().character);
            }
        }
    }
}
