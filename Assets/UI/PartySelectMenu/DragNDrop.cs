﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DragNDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField] Canvas canvas;

    public Character character;

    RectTransform rectTransform;

    CanvasGroup canvasGroup;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        Debug.Log("Pointer Down");
    }

    public void OnDrag(PointerEventData pointerEventData)
    {
        Debug.Log("Dragging");
        rectTransform.anchoredPosition += pointerEventData.delta / canvas.scaleFactor;
    }

    public void OnBeginDrag(PointerEventData pointerEventData)
    {
        Debug.Log("Begin Drag");
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnEndDrag(PointerEventData pointerEventData)
    {
        Debug.Log("End Drag");
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }
}
