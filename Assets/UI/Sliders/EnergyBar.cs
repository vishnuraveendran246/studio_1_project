﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
	public Slider slider;
	public Gradient gradient;
	public Image fill;

	public Text value;

	Character hold;

	private void Start()
	{
		if (!slider)
			slider.maxValue = BattleManager.instance.currentChar.baseStats.energy;

		fill.color = gradient.Evaluate(1f);
	}

	private void Update()
	{
		if (BattleManager.instance.currentChar != null)
			hold = BattleManager.instance.currentChar;

		slider.value = hold.energy;

		fill.color = gradient.Evaluate(slider.normalizedValue);

		value.text = hold.energy.ToString();
	}
}