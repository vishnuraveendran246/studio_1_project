﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VictoryScript : MonoBehaviour
{
    public float timeFactor = 1;
    Image img;
    Button button;
    Color col;
    float time;
    bool disabled;

    // Start is called before the first frame update
    void Start()
    {
        img = GetComponent<Image>();
        button = GetComponent<Button>();
        col = img.color;
        col.a = 0;
        img.color = col;
        disabled = true;
        img.enabled = false;
        button.enabled = false;
    }

    public void Victory()
    {
        if (disabled)
        {
            disabled = false;
            img.enabled = true;
            time = 0;
            StartCoroutine(LerpIn());
        }
    }

    public void Close()
    {
        if (!disabled)
        {
            disabled = true;
            time = 0;
            StartCoroutine(LerpOut());
        }
    }

    IEnumerator LerpIn()
    {
        while (time <= 1)
        {
            time += Time.deltaTime * timeFactor;

            col.a = Mathf.Lerp(0, 1, time*time);
            img.color = col;
            yield return null;
        }
        button.enabled = true;
    }

    IEnumerator LerpOut()
    {
        while (time <= 1)
        {
            time += Time.deltaTime * timeFactor;

            col.a = Mathf.Lerp(1, 0, time * time);
            img.color = col;
            yield return null;
        }
        button.enabled = false;
        img.enabled = false; 
        if (BattleManager.instance.winCount == 1 || BattleManager.instance.winCount == 3)
        {
            StorySystem.instance.PlayStory();
        }
        if (BattleManager.instance.winCount == 5)
            SceneManager.LoadSceneAsync("EndingScene", LoadSceneMode.Single);
    }
}
