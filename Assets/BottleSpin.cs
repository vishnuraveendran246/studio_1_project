﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleSpin : MonoBehaviour
{
    Rigidbody rb;
    bool start = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    // Update is called once per frame
    void Update()
    {
        if (!start)
        {
            rb.AddTorque(new Vector3(1, 1, 1) * 30, ForceMode.Impulse);
            start = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GetComponentInParent<ProjectileMove>().Delete(collision);
    }
}
